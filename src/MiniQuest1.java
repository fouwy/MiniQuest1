import java.util.Random;
import java.util.Scanner;

public class MiniQuest1 {

	public static void main(String[] args) {

		//store maze
		char[][] maze = {	{'X','X','X','X','X','X','X','X','X','X'}, 
							{'X','H',' ',' ',' ',' ',' ',' ',' ','X'},
							{'X',' ','X','X',' ','X',' ','X',' ','X'},
							{'X','D','X','X',' ','X',' ','X',' ','X'},
							{'X',' ','X','X',' ','X',' ','X',' ','X'},
							{'X',' ',' ',' ',' ',' ',' ','X',' ','X'},
							{'X',' ','X','X',' ','X',' ','X',' ','X'},
							{'X',' ','X','X',' ','X',' ','X',' ','X'},
							{'X','K','X','X',' ',' ',' ',' ',' ','X'},
							{'X','X','X','X','X','X','X','X','X','X'}
						};
		
		//Game loop
		int hx, hy;
		int randSide, randSquare;
		boolean hero_has_key = false, exit_is_placed = false;
		
		Random rand = new Random();
		Scanner s = new Scanner(System.in);
		
		char uc;
		
		hx = 1;
		hy = 1;
		
		//randomly generate exit
		
		do {
			randSide = rand.nextInt(4);
			randSquare = rand.nextInt(8) + 1;
			
			switch (randSide) {
			case 0: //TOP SIDE
				if (maze[1][randSquare] == ' ' &&
					maze[1][randSquare+1] != 'D' &&
					maze[1][randSquare+1] != 'H' &&
					maze[1][randSquare+1] != 'K' &&
					maze[1][randSquare-1] != 'D' &&
					maze[1][randSquare-1] != 'D' &&
					maze[1][randSquare-1] != 'H' &&
					maze[1][randSquare-1] != 'K') { //check if adjacent squares are free
					
					maze[0][randSquare] = 'E';
					exit_is_placed = true;
				}
				break;
				
			case 1: //RIGHT SIDE
				if (maze[randSquare][8] == ' ' &&
						maze[randSquare+1][8] != 'D' &&
						maze[randSquare+1][8] != 'H' &&
						maze[randSquare+1][8] != 'K' &&
						maze[randSquare-1][8] != 'D' &&
						maze[randSquare-1][8] != 'D' &&
						maze[randSquare-1][8] != 'H' &&
						maze[randSquare-1][8] != 'K') {
					
					maze[randSquare][9] = 'E';
					exit_is_placed = true;
				}
				break;
				
			case 2: //BOTTOM SIDE
				if (maze[8][randSquare] == ' ' &&
					maze[8][randSquare+1] != 'D' &&
					maze[8][randSquare+1] != 'H' &&
					maze[8][randSquare+1] != 'K' &&
					maze[8][randSquare-1] != 'D' &&
					maze[8][randSquare-1] != 'D' &&
					maze[8][randSquare-1] != 'H' &&
					maze[8][randSquare-1] != 'K') {
					
					maze[9][randSquare] = 'E';
					exit_is_placed = true;
				}
				break;
				
			case 3: //LEFT SIDE
				if (maze[randSquare][1] == ' ' &&
					maze[randSquare+1][1] != 'D' &&
					maze[randSquare+1][1] != 'H' &&
					maze[randSquare+1][1] != 'K' &&
					maze[randSquare-1][1] != 'D' &&
					maze[randSquare-1][1] != 'D' &&
					maze[randSquare-1][1] != 'H' &&
					maze[randSquare-1][1] != 'K') {
					
					maze[randSquare][0] = 'E';
					exit_is_placed = true;
				}
				break;
			}
			
		} while (exit_is_placed==false);
		
		do {
			//print maze
			printMaze(maze);
			
			System.out.print("cmd>");
			uc = s.next().charAt(0);
			switch(uc) {
			case 'w': //UP
				if (maze[hx-1][hy] == ' ') {	//check for free space
					maze[hx-1][hy] = 'H';
					maze[hx][hy] = ' ';
					hx = hx - 1;
				}
				
				else if (maze[hx-1][hy] == 'K') {
					maze[hx-1][hy] = 'H';
					maze[hx][hy] = ' ';
					hx = hx - 1;
					hero_has_key = true;
				}
				
				else if (maze[hx-1][hy] == 'E') {
					if (hero_has_key == true) {
						maze[hx-1][hy] = 'E';
						maze[hx][hy] = ' ';
						hx = hx - 1;
					}
				}
				break;
			case 's': //DOWN
				if (maze[hx+1][hy] == ' ') {	//check for free space
					maze[hx+1][hy] = 'H';
					maze[hx][hy] = ' ';
					hx = hx + 1;
				}
				else if (maze[hx+1][hy] == 'K') {
					maze[hx+1][hy] = 'H';
					maze[hx][hy] = ' ';
					hx = hx + 1;
					hero_has_key = true;
				}
				
				else if (maze[hx+1][hy] == 'E') {
					if (hero_has_key == true) {
						maze[hx+1][hy] = 'E';
						maze[hx][hy] = ' ';
						hx = hx + 1;
						
					}
				}
				break;
			case 'd': //LEFT
				if (maze[hx][hy+1] == ' ') {	//check for free space
					maze[hx][hy+1] = 'H';
					maze[hx][hy] = ' ';
					hy = hy+1;
				}
				else if (maze[hx][hy+1] == 'K') {
					maze[hx][hy+1] = 'H';
					maze[hx][hy] = ' ';
					hy = hy + 1;
					hero_has_key = true;
				}
				
				else if (maze[hx][hy+1] == 'E') {
					if (hero_has_key == true) {
						maze[hx][hy+1] = 'E';
						maze[hx][hy] = ' ';
						hy = hy+1;
						
					}
				}
				break;
			case 'a': //RIGHT
				if (maze[hx][hy-1] == ' ') {	//check for free space
					maze[hx][hy-1] = 'H';
					maze[hx][hy] = ' ';
					hy = hy-1;
				}
				else if (maze[hx][hy-1] == 'K') {
					maze[hx][hy-1] = 'H';
					maze[hx][hy] = ' ';
					hy = hy - 1;
					hero_has_key = true;
				}
				
				else if (maze[hx][hy-1] == 'E') {
					if (hero_has_key == true) {
						maze[hx][hy-1] = 'E';
						maze[hx][hy] = ' ';
						hy = hy-1;
						
					}
				}
				break;
			}
			
			//check if Hero escaped
			if (maze[hx][hy] == 'E') {
				printMaze(maze);
				System.out.println("YOU WIN!");
				break;
			}
			
			//check if square is adjacent to Dragon
			else if (maze[hx-1][hy] == 'D' | maze[hx+1][hy] == 'D' |
				maze[hx][hy-1] == 'D' | maze[hx][hy+1] == 'D' ) {
				
				printMaze(maze);
				System.out.println("YOU DIED");
				break;
			}
			
		} while(uc != 'q');
		
		System.out.print("Game Over");
		s.close();
		
	}

	private static void printMaze(char[][] maze) {
		for (int i=0; i < maze.length; i++) {
			for(int j=0; j < maze[0].length; j++)
				System.out.print(maze[i][j] + " ");
		
			System.out.println();
		}
	}
}
